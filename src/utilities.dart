
import 'dart:io';

import 'package:markdown/markdown.dart';

import 'consumers.dart';
import 'directives.dart';
import 'globals.dart';
import 'misc.dart';
import 'read.dart';
import 'runes.dart';
import 'package:path/path.dart' as p;

void processFile(FileSystemEntity sourceFile)
{
    if (sourceFile.statSync().type != FileSystemEntityType.file) return;

    var sourceFilePath = sourceFile.path;

    var outputFilePathParts =
        p.join(
            outputFolderPath,
            // +1 to ignore the separator and make `join` work.
            // `join` will ignore earlier parts if a part is absolute.
            // absolute = starts with path separator
            sourceFilePath.substring(sourceFolderPath.length + 1))
        .split(Platform.pathSeparator);

    blockMap = {};
    userVarMap = {};
    extendsRef = "";

    print("Processing: $sourceFilePath");
    readFile(sourceFilePath);

    StringBuffer outputFileBuffer = StringBuffer();

    switch (activeFile.ext)
    {
        case ".html":
            outputFileBuffer = buildOutputFile(false, FileTypeEnum.fileTypeHTML);
            break;
        case ".md":
            outputFilePathParts[outputFilePathParts.length - 1] = "${activeFile.filename}.html";
            outputFileBuffer = buildOutputFile(false, FileTypeEnum.fileTypeMarkdown);
            break;
        default:
            // Do nothing.
    }

    var outputFilePath = outputFilePathParts.join(Platform.pathSeparator);

    Directory(File(outputFilePath).absolute.parent.path).createSync(recursive: true);

    print("Writing file: $outputFilePath");
    if (outputFileBuffer.isNotEmpty)
    {
        File(outputFilePath).writeAsStringSync(outputFileBuffer.toString());
    }
    else
    {
        File(sourceFilePath).copySync(outputFilePath);
    }

    print("");
}

String trimString(String str, int newLength)
{
    var newStr = "";

    for(var i = 0; i < str.length; i++)
    {
        var pos = i;
        var char = str[i];

        if (pos >= newLength)
        {
            break;
        }

        newStr += char;
    }

    return newStr;
}

StringBuffer buildOutputFile(bool template, FileTypeEnum fileType)
{
    var buffer = StringBuffer();

    while(true)
    {
        try
        {
            var readDirectiveInParts_ = readDirectiveInParts(fileType).item1;
            var indentStr = calculateIndentStr();

            if (readDirectiveInParts_[0] != "include")
            {
                consumeNewlineIfExists();
            }

            templateDependentDirectiveProcessor(buffer, readDirectiveInParts_, template, indentStr);
            templateIndependentDirectiveProcessor(buffer, readDirectiveInParts_, indentStr);
        }
        on EOF
        {
            break;
        }
        on NotDirectiveStartErr
        {
            try
            {
                buffer.write(readRune().item1);
            }
            on EOF
            {
                break;
            }
        }
    }

    if (fileType == FileTypeEnum.fileTypeMarkdown)
    {
        buffer = StringBuffer(markdownToHtml(buffer.toString(), extensionSet: ExtensionSet.gitHubFlavored));
        lastMdFileAsHTMLBuffer = buffer;
    }

    if (extendsRef != "")
    {
        // Open template being extended
        readFile(p.join(templatesFolderPath, extendsRef));
        extendsRef = "";
        return buildOutputFile(true, FileTypeEnum.fileTypeHTML);
    }

    return buffer;
}

String calculateIndentStr()
{
    var indentStr = "";
    var i = 0;
    while (i < whitespaceCount)
    {
        indentStr += indentCharacter;
        i++;
    }
    return indentStr;
}

String buildError(String msg)
{
    return "At line [${activeFile.linesRead}] in file [${activeFile.path}] with message [$msg]";
}

String getIncludeTemplate(String key)
{
    if (includeMap.containsKey(key))
    {
        return includeMap[key]!;
    }

    // using this because I don't expect includes to be very big
    var htmlToInclude = File(p.join(templatesFolderPath, key)).readAsStringSync();
    return includeMap[key] = htmlToInclude;
}

String getUserDefinedVar(String readVarKey)
{
    var varRetrievalKey = getVarRetrievalKey(readVarKey);

    if (userVarMap.containsKey(varRetrievalKey))
    {
        return userVarMap[varRetrievalKey]!;
    }

    throw Exception(buildError("$readVarKey variable not defined"));
}

String getVarRetrievalKey(String varReadKey) // $title -> title:
{
    if (varReadKey.length < 2)
    {
        throw Exception("$varReadKey incorrect variable format");
    }

    return "${varReadKey.substring(1)}:";
}

void writeStringToBuffer(StringBuffer buffer, String stringToWrite, String indentStr)
{
    var firstLine = true;
    StringBuffer lineBuffer = StringBuffer();

    void writeLineBufferToBuffer()
    {
        var lineBufferAsStr = lineBuffer.toString();

        if (!lineBufferAsStr.equals(newline) && !lineBufferAsStr.equals(""))
        {
            if (!firstLine)
            {
                buffer.write(indentStr);
            }
            buffer.write(lineBufferAsStr.toString());
            firstLine = false;
        }

        lineBuffer.clear();
    }

    for(var i = 0; i < stringToWrite.length; i++)
    {
        var readRune = stringToWrite[i];
        lineBuffer.write(readRune);
        if (readRune == '\n') writeLineBufferToBuffer();
    }

    if (lineBuffer.isNotEmpty)
    {
        writeLineBufferToBuffer();
    }
}

void unreadBytes(int count)
{
    activeFile.osFile.setPositionSync(activeFile.osFile.positionSync() - count);
}