import 'dart:convert';

import 'package:tuple/tuple.dart';

import 'globals.dart';
import 'misc.dart';
import 'utilities.dart';

int getRuneSizeInBytes(String r)
{
    return Utf8Encoder().convert(r).length;
}

Tuple2<String, int> readRune()
{
    return readRuneAlt(1, false);
}

Tuple2<String, int> readRunes(int amount)
{
    String readRunesStr = "";
    int totalBsReadCount = 0;

    while (amount > 0)
    {
        var readRuneTuple = readRune();

        var readRune_ = readRuneTuple.item1;
        var bsReadCount = readRuneTuple.item2;

        readRunesStr += readRune_;
        totalBsReadCount += bsReadCount;
        amount--;
    }

    return Tuple2(readRunesStr, totalBsReadCount);
}

Tuple2<String, int> readRuneAlt(int codePointBsSize, bool peekMode)
{
    if (codePointBsSize < 1)
    {
        codePointBsSize = 1;
    }

    var currentFilePos = activeFile.osFile.positionSync();
    var fileLength = activeFile.osFile.lengthSync();
    if (currentFilePos == fileLength)
    {
        throw EOF();
    }

    var bs = activeFile.osFile.readSync(codePointBsSize);
    var calculatedCodePointBsSize = 1;

    if ((bs[0] & 0x80) == 0x80) // multi byte code point
    {
        if ((bs[0] & 0x40) == 0x40)
        {
            calculatedCodePointBsSize++;

            if ((bs[0] & 0x20) == 0x20)
            {
                calculatedCodePointBsSize++;

                if ((bs[0] & 0x10) == 0x10)
                {
                    calculatedCodePointBsSize++;
                }
            }
        }
    }

    if (calculatedCodePointBsSize > 1 && codePointBsSize != calculatedCodePointBsSize)
    {
        unreadBytes(codePointBsSize);
        return readRuneAlt(calculatedCodePointBsSize, peekMode);
    }

    var readRune = Utf8Decoder().convert(bs);

    if (!peekMode)
    {
        if (readRune == '\n')
        {
            activeFile.linesRead = activeFile.linesRead+ 1;
            countWhitespace = true;
            whitespaceCount = 0;
        }
        else if ((readRune == ' ' || readRune == '\t') && countWhitespace)
        {
            indentCharacter = readRune;
            whitespaceCount++;
        }
        else
        {
            countWhitespace = false;
        }
    }

    return Tuple2(readRune, codePointBsSize);
}

String? peekRuneN(int n)
{
    String readRunes = "";
    int totalBsReadCount = 0;

    while (n > 0)
    {
        Tuple2<String, int> readRuneAltTuple;

        try
        {
            readRuneAltTuple = readRuneAlt(1, true);
        }
        on EOF // Capture. We are looking ahead. It hasn't actually happened yet.
        {
            unreadBytes(totalBsReadCount);
            return null;
        }

        var readRune = readRuneAltTuple.item1;
        var bsReadCount = readRuneAltTuple.item2;

        totalBsReadCount += bsReadCount;
        readRunes += readRune;

        n--;
    }

    unreadBytes(totalBsReadCount);

    return readRunes;
}

void expectRune(String runeToExpect)
{
    try
    {
        var peekedRune = peekRuneN(1);
        if (peekedRune == runeToExpect)
        {
            readRune();
        }
        else
        {
            throw Exception(buildError("Expected $runeToExpect but found $peekedRune instead"));
        }
    }
    on EOF
    {
        print(buildError("Expected $runeToExpect but reached end of file instead"));
        rethrow;
    }
}