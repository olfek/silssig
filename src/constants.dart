const htmlDirectivePattern     = "{%";
const mdDirectivePattern       = "#%";
const htmlBlockStartDirective  = "block";
const htmlBlockEndDirective    = "endblock";
const htmlExtendsDirective     = "extends";
const mdExtendsDirective       = "container:";
const htmlInsertHereDirective  = "insertHere";
const htmlIncludeDirective     = "include";