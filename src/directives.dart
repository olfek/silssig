import 'package:tuple/tuple.dart';

import 'constants.dart';
import 'globals.dart';
import 'misc.dart';
import 'read.dart';
import 'runes.dart';
import 'utilities.dart';

String readDirective(FileTypeEnum fileType)
{
    var isDirectiveStart_ = isDirectiveStart(fileType);

    if (!isDirectiveStart_)
    {
        throw NotDirectiveStartErr(); // Non fatal error
    }

    StringBuffer directiveReadBuffer = StringBuffer();
    var totalBsReadCount = Ref<int>(0);

    if (fileType == FileTypeEnum.fileTypeHTML)
    {
        expectRune('{');
        expectRune('%');

        try
        {
            readIntoBufferUntil(directiveReadBuffer, '%', totalBsReadCount);
        }
        on EOF
        {
            unreadBytes(totalBsReadCount.value + getRuneSizeInBytes('{') + getRuneSizeInBytes('%'));
            throw NotDirectiveStartErr();
        }

        expectRune('}');
    }
    else if (fileType == FileTypeEnum.fileTypeMarkdown)
    {
        expectRune('#'); // skip #
        expectRune('%'); // skip %

        try
        {
            readIntoBufferUntil(directiveReadBuffer, '\n', totalBsReadCount);
        }
        on EOF
        {
            unreadBytes(totalBsReadCount.value + getRuneSizeInBytes('#') + getRuneSizeInBytes('%'));
            throw NotDirectiveStartErr();
        }
    }

    return directiveReadBuffer.toString().trim();
}

bool isDirectiveInputVar(String directive)
{
    var directiveLen = directive.length;

    if (directiveLen < 2)
    {
        return false;
    }

    if (directive[directiveLen-1] != ':')
    {
        return false;
    }

    return true;
}

bool isDirectiveOutputVar(String directive)
{
    var directiveLen = directive.length;

    if (directiveLen < 2)
    {
        return false;
    }

    if (directive[0] != '\$')
    {
        return false;
    }

    return true;
}

void templateDependentDirectiveProcessor( // if readeMode being false is the equivalent of write mode
    StringBuffer buffer,
    List<String> readDirectiveInParts,
    bool template,
    String indentStr)
{
    if (readDirectiveInParts.isEmpty)
    {
        throw EmptyDirectiveErr();
    }

    var directive = readDirectiveInParts[0];

    if (
        directive == htmlBlockStartDirective &&
        (template && extendsRef == "") &&
        readDirectiveInParts.length == 2)
    {
        if (!blockMap.containsKey(readDirectiveInParts[1]))
        {
            throw Exception(buildError("$readDirectiveInParts[1] block not defined"));
        }

        writeStringToBuffer(buffer, blockMap[readDirectiveInParts[1]]!, indentStr);
        return;
    }

    if (
        (directive == htmlExtendsDirective || directive == mdExtendsDirective) &&
        readDirectiveInParts.length == 2)
    {
        extendsRef = readDirectiveInParts[1];
        return;
    }

    if (
        directive == htmlBlockStartDirective &&
        readDirectiveInParts.length == 2)
    {
        readBlock(readDirectiveInParts[1], indentStr);
        return;
    }

    if (
        directive == htmlInsertHereDirective &&
        template)
    {
        writeStringToBuffer(
            buffer,
            lastMdFileAsHTMLBuffer.toString(),
            indentStr,
        );
        return;
    }
}

void templateIndependentDirectiveProcessor(StringBuffer buffer, List<String> readDirectiveInParts, String indentStr)
{
    if (readDirectiveInParts.isEmpty)
    {
        throw EmptyDirectiveErr();
    }

    var directive = readDirectiveInParts[0];

    if (isDirectiveInputVar(directive) && readDirectiveInParts.length == 2)
    {
        userVarMap[directive] = readDirectiveInParts[1].replaceAll("-", " ");
    }
    else if (isDirectiveOutputVar(directive))
    {
        buffer.write(getUserDefinedVar(directive));
    }
    else if (directive == htmlIncludeDirective && readDirectiveInParts.length == 2)
    {
        var includeTemplate = getIncludeTemplate(readDirectiveInParts[1]);
        writeStringToBuffer(buffer, includeTemplate, indentStr);
    }
}

Tuple2<List<String>, String> readDirectiveInParts(FileTypeEnum fileType)
{
    var readDirective_ = readDirective(fileType);
    return Tuple2(readDirective_.split(" "), readDirective_);
}

bool isDirectiveStart(FileTypeEnum fileType)
{
    var runesPeeked = peekRuneN(2);

    String directiveStartPattern;

    if (fileType == FileTypeEnum.fileTypeHTML)
    {
        directiveStartPattern = htmlDirectivePattern;
    }
    else if (fileType == FileTypeEnum.fileTypeMarkdown)
    {
        directiveStartPattern = mdDirectivePattern;
    }
    else
    {
        throw Exception("$fileType unknown");
    }

    return runesPeeked == directiveStartPattern;
}