class NotDirectiveStartErr implements Exception {}
class EOF implements Exception {}
class EmptyDirectiveErr implements Exception {}

extension StringEquals on String
{
    bool equals(String other)
    {
        return this == other;
    }
}

enum FileTypeEnum
{
    fileTypeMarkdown,
    fileTypeHTML
}

class Ref<T>
{
    T value;

    Ref(this.value);
}