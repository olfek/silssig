import 'active_file.dart';

bool countWhitespace = true;
int whitespaceCount = 0;

late Map<String, String> blockMap;
late Map<String, String> includeMap;
late Map<String, String> userVarMap;

late ActiveFile activeFile;

late String extendsRef;
late StringBuffer lastMdFileAsHTMLBuffer;
late String indentCharacter;

late final String newline;
late final String templatesFolderPath;
late final String outputFolderPath;
late final String sourceFolderPath;