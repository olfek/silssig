import 'globals.dart';
import 'runes.dart';

void consumeNewlineIfExists()
{
    consumeRunesIfExists(newline);
}

void consumeRuneIfExists(String toConsume) // not used atm
{
    if (peekRuneN(1) == toConsume)
    {
        readRune();
    }
}

void consumeRunesIfExists(String toConsume)
{
    var peekedRunesAsBs = peekRuneN(toConsume.length);

    if (peekedRunesAsBs == toConsume)
    {
        readRunes(toConsume.length);
    }
}