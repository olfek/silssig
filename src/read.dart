import 'dart:io';

import 'active_file.dart';
import 'constants.dart';
import 'directives.dart';
import 'globals.dart';
import 'misc.dart';
import 'runes.dart';
import 'utilities.dart';

void readFile(String path)
{
    if (!File(path).existsSync())
    {
        throw Exception("$path is not a file");
    }

    RandomAccessFile openedFile;

    try
    {
        openedFile = File(path).openSync();
    }
    on FileSystemException
    {
        throw Exception("Could not open file: $path\n");
    }

    activeFile = ActiveFile(path, openedFile);
}

void readIntoBufferUntil(StringBuffer directiveReadBuffer, String untilRune, Ref<int> totalBsReadCount)
{
    while(true)
    {
        var readRuneTuple = readRune();

        var readRune_ = readRuneTuple.item1;
        var bsReadCount = readRuneTuple.item2;

        totalBsReadCount.value += bsReadCount;

        if (readRune_ == untilRune)
        {
            break;
        }

        directiveReadBuffer.write(readRune_);
    }
}

void readBlock(String blockName, String indentStr)
{
    StringBuffer blockBuffer = StringBuffer();

    while (true)
    {
        var directiveMatched = false;
        var readMore = false;

        try
        {
            var readDirectiveInPartsTuple = readDirectiveInParts(FileTypeEnum.fileTypeHTML);

            var readDirectiveInParts_ = readDirectiveInPartsTuple.item1;
            var directiveRead = readDirectiveInPartsTuple.item2;

            directiveMatched = directiveRead == htmlBlockEndDirective;
            // TODO: Recursive expansion of read blocks.
            if (readDirectiveInParts_.length >= 2 && blockMap.containsKey(readDirectiveInParts_[1]))
            {
                writeStringToBuffer(blockBuffer, blockMap[readDirectiveInParts_[1]]!, indentStr);
            }
        }
        on EOF
        {
            break;
        }
        on NotDirectiveStartErr
        {
            readMore = true;
            // continue;
        }

        if (readMore || !directiveMatched)
        {
            try
            {
                blockBuffer.write(readRune().item1);
            }
            on EOF
            {
                break;
            }
        }
        else
        {
            // blockReadSuccess
            // If not set - we expect panic when an attempt is made to read it.
            blockMap[blockName] = blockBuffer.toString();
            break;
        }
    }
}