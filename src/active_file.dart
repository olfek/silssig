import 'dart:io';
import 'package:path/path.dart' as p;

import 'utilities.dart';

class ActiveFile
{
    String path;
    RandomAccessFile osFile;
    String ext;
    String filenameIncExt;
    int linesRead;
    late String filename;

    ActiveFile(this.path, this.osFile) :
        ext = p.extension(path).toLowerCase(),
        filenameIncExt = p.basename(path),
        linesRead = 1
    {
        filename = trimString(filenameIncExt, filenameIncExt.length - ext.length);
    }
}