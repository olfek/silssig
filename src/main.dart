import 'dart:io';
import 'package:args/args.dart';
import 'package:path/path.dart' as p;

import 'globals.dart';
import 'utilities.dart';

void main(List<String> rawArgs)
{
    ArgResults args;
    var parser = ArgParser();
    parser.addOption('WorkingDirectoryPath', mandatory: true);
    parser.addOption('OutputFolderPath', mandatory: true);
    parser.addOption('OutputLineFeed', mandatory: true, allowed: ['crlf', 'lf']);

    try
    {
        args = parser.parse(rawArgs);
    }
    on FormatException catch(e)
    {
        print("");
        print(e.message);
        print("");
        print(parser.usage);
        print("");
        exit(1);
    }

    var workingDirectoryPath = p.normalize(args['WorkingDirectoryPath'] as String);
    outputFolderPath = p.normalize(args['OutputFolderPath'] as String);
    var outputLineFeed = (args['OutputLineFeed'] as String).toLowerCase();

    if (outputLineFeed == "crlf")
    {
        newline = "\r\n";
    }
    else if (outputLineFeed == "lf")
    {
        newline = "\n";
    }

    includeMap = {};

    print("");

    templatesFolderPath = p.join(workingDirectoryPath, 'templates');
    sourceFolderPath = p.join(workingDirectoryPath, 'src');

    for (var file in Directory(sourceFolderPath).listSync(recursive: true))
    {
        processFile(file);
    }

    print("Success 🥳\n");
}